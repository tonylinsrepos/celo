package com.celo.conversationapp

import com.celo.conversationapp.models.Case
import com.celo.conversationapp.models.Conversation

fun buildMockConversations(): List<Pair<Conversation, String>> {
    return listOf(
        Pair(Conversation(0, "General"), "I'm busy with a customer at the moment."),
        Pair(Conversation(0, "Engineering talk"), "The bug fixes are still a while away, let's discuss."),
        Pair(Conversation(0, "Status reports"), "The server had an outage last night, for about 2 hours"),
        Pair(Conversation(0, "Testing"), "This is a test, askjsdgsd8gs sjdig"),
        Pair(Conversation(0, "Bob Jones"), "We discussed it yesterday in a meeting with backend."),
        Pair(Conversation(0, "Funny pictures"), "That was great!"),
        Pair(Conversation(0, "General"), "I'm busy with a customer at the moment."),
        Pair(Conversation(0, "Engineering talk"), "The bug fixes are still a while away, let's discuss."),
        Pair(Conversation(0, "Status reports"), "The server had an outage last night, for about 2 hours"),
        Pair(Conversation(0, "Testing"), "This is a test, askjsdgsd8gs sjdig"),
        Pair(Conversation(0, "Bob Jones"), "We discussed it yesterday in a meeting with backend."),
        Pair(Conversation(0, "Funny pictures"), "That was great!"),
    )
}

fun buildMockCases(): List<Pair<Case, String>> {
        return listOf(
            Pair(Case(0, "Paediatric follow up", "A1"), "Let's circle back to this next week."),
            Pair(Case(0, "Outpatient clinic", "A2"), "I won't be available this week as I'm on holiday, but feel free to email me."),
            Pair(Case(0, "Dermatology photos", "A3"), "Photo"),
            Pair(Case(0, "Blood donor", "A4"), "The clinic will be open 9am to 5pm Monday through Friday."),
            Pair(Case(0, "Patient follow-up", "A5"), "According to last weeks scans, everything is looking good!"),
            Pair(Case(0, "Kim Smith physio", "A6"), "I'll see them this afternoon."),
            Pair(Case(0, "Paediatric follow up", "A7"), "Let's circle back to this next week."),
            Pair(Case(0, "Outpatient clinic", "A8"), "I won't be available this week as I'm on holiday, but feel free to email me."),
            Pair(Case(0, "Dermatology photos", "A9"), "Photo"),
            Pair(Case(0, "Blood donor", "A10"), "The clinic will be open 9am to 5pm Monday through Friday."),
            Pair(Case(0, "Patient follow-up", "A11"), "According to last weeks scans, everything is looking good!"),
            Pair(Case(0, "Kim Smith physio", "A12"), "I'll see them this afternoon."),
        )
    }