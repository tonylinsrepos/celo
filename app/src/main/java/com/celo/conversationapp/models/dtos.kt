package com.celo.conversationapp.models

data class ConversationWithLatestMessage(
    val conversation: IConversation,
    val latestMessage: Message?
) {
    fun getMessageToDisplay(): String {
        return this.latestMessage?.content ?: "---"
    }
}
