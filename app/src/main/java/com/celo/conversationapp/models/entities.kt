package com.celo.conversationapp.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "conversations")
data class Conversation(
    @PrimaryKey(autoGenerate = true)
    override val id: Long = 0,

    @ColumnInfo(index = true)
    override val name: String,

    ) : IConversation

@Entity(tableName = "cases")
data class Case(
    @PrimaryKey(autoGenerate = true)
    override val id: Long = 0,

    @ColumnInfo(index = true)
    override val name: String,

    @ColumnInfo(index = true)
    val patientId: String,
) : IConversation

@Entity(tableName = "messages", indices = [androidx.room.Index(value = ["ownerId", "ownerType"])])
data class Message(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,

    val ownerId: Long,

    val ownerType: Int,  //conversation: 0, case: 1

    val senderName: String,

    val content: String,

    val timeStamp: Long
)

interface IConversation {
    val id: Long
    val name: String
}