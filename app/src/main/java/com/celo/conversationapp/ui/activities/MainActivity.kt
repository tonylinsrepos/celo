package com.celo.conversationapp.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavDestination
import androidx.navigation.Navigation.findNavController
import androidx.navigation.ui.NavigationUI.setupWithNavController
import com.celo.conversationapp.R
import com.celo.conversationapp.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        this.binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setDisplayShowHomeEnabled(true)
            supportActionBar?.title = context.getString(R.string.your_conversations)
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        this.setupNavigation()
    }

    private fun setupNavigation() {
        val navController = findNavController(this, R.id.nav_host_fragment).also {
            it.addOnDestinationChangedListener { _, destination, _ ->
                this.handleNavigationDestination(destination)
            }
        }

        setupWithNavController(this.binding.mainBottomNav, navController)
    }

    /**
     * Hide the BottomNavigationView when navigate to MessageFragment
     */
    private fun handleNavigationDestination(destination: NavDestination) {
        if (isTopLevelDestination(destination))
            showTopLevelDestination()
        else
            showOtherLevelDestination()
    }

    private fun isTopLevelDestination(destination: NavDestination): Boolean {
        return when (destination.id) {
            R.id.navigation_basic,
            R.id.navigation_extra -> true
            else -> false
        }
    }

    private fun showTopLevelDestination() {

        this.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        this.supportActionBar?.title = getString(R.string.your_conversations)

        this.binding.mainBottomNav.visibility = View.VISIBLE
    }

    private fun showOtherLevelDestination() {
        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.binding.mainBottomNav.visibility = View.GONE
    }

    fun getToolbar() = this.binding.toolbar

    companion object {
        fun start(activity: Activity) {
            Intent(activity, MainActivity::class.java).also {
                activity.startActivity(it)
            }
        }
    }

}