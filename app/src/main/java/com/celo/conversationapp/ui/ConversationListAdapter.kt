package com.celo.conversationapp.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.celo.conversationapp.R
import com.celo.conversationapp.databinding.ConversationCardBinding
import com.celo.conversationapp.models.Case
import com.celo.conversationapp.models.ConversationWithLatestMessage

class ConversationListAdapter
    : ListAdapter<ConversationWithLatestMessage, ConversationListAdapter.ConversationItemViewHolder>(ConversationItemDiffCallback()) {

    var onItemClicked: ((ConversationWithLatestMessage) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConversationItemViewHolder {
        val binding: ConversationCardBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.conversation_card,
            parent,
            false
        )

        return ConversationItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ConversationItemViewHolder, position: Int) {
        val postItem = getItem(position)

        holder.itemView.setOnClickListener {
            onItemClicked?.invoke(postItem)
        }

        holder.bind(postItem)
    }

    inner class ConversationItemViewHolder(
        private val binding: ConversationCardBinding,
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(conversationItem: ConversationWithLatestMessage) {
            binding.conversation = conversationItem
            val colorRes = when (conversationItem.conversation) {
                is Case -> {
                    binding.patientId = "Patient ID: ${conversationItem.conversation.patientId}"
                    androidx.appcompat.R.color.material_deep_teal_200
                }
                else -> {
                    binding.patientId = null
                    androidx.appcompat.R.color.material_grey_50
                }
            }
            if (onItemClicked == null) {
                binding.nameLabel.setCompoundDrawables(null, null, null, null)
            }
            binding.root.setBackgroundResource(colorRes)
            binding.executePendingBindings()
        }
    }
}

private class ConversationItemDiffCallback : DiffUtil.ItemCallback<ConversationWithLatestMessage>() {
    override fun areItemsTheSame(oldItem: ConversationWithLatestMessage, newItem: ConversationWithLatestMessage): Boolean {
        return oldItem.conversation.id == newItem.conversation.id
    }

    override fun areContentsTheSame(oldItem: ConversationWithLatestMessage, newItem: ConversationWithLatestMessage): Boolean {
        return oldItem.conversation.id == newItem.conversation.id
    }

}