package com.celo.conversationapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.navGraphViewModels
import com.celo.conversationapp.R
import com.celo.conversationapp.databinding.FragmentConversationsBinding
import com.celo.conversationapp.models.ConversationWithLatestMessage
import com.celo.conversationapp.ui.ConversationListAdapter
import com.celo.conversationapp.viewModels.BasicConversationViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ConversationsFragment : Fragment() {

    private val viewModel: BasicConversationViewModel by navGraphViewModels(R.id.main_nav_graph_xml) {
        defaultViewModelProviderFactory
    }

    private lateinit var binding: FragmentConversationsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        this.binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_conversations,
            container, false
        )

        this.binding.lifecycleOwner = this.viewLifecycleOwner
        this.binding.viewModel = this.viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //populate the all or filtered posts
        this.viewModel.conversationsAndCasesToDisplay.observe(viewLifecycleOwner) { conversations ->
            setupListView(conversations)
        }
    }

    private fun setupListView(conversationWithLatestMessages: List<ConversationWithLatestMessage>) {
        ConversationListAdapter().also { adapter ->
            this.binding.conversationList.adapter = adapter
            adapter.submitList(conversationWithLatestMessages)
        }
    }
}