package com.celo.conversationapp.ui.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.celo.conversationapp.R


class NewConversationDialog(private val onCreateClicked: (String) -> Unit) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val editText = EditText(context).apply {
            this.hint = "name of the conversation"
        }
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setMessage(R.string.dialog_new_conversation)
                .setView(editText)
                .setPositiveButton(R.string.okay,
                    DialogInterface.OnClickListener { dialog, id ->
                        editText.text.trim().takeIf { it.isNotBlank() }?.let {
                            onCreateClicked.invoke(it.toString())
                        }
                    })
                .setNegativeButton(R.string.cancel,
                    DialogInterface.OnClickListener { dialog, id ->
                        // User cancelled the dialog
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}
