package com.celo.conversationapp.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.celo.conversationapp.R
import com.celo.conversationapp.databinding.MessageCardBinding
import com.celo.conversationapp.models.Message

class MessageListAdapter
    : ListAdapter<Message, MessageListAdapter.MessageItemViewHolder>(MessageItemDiffCallback()) {

    var onItemClicked: ((Message) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageItemViewHolder {
        val binding: MessageCardBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.message_card,
            parent,
            false
        )

        return MessageItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MessageItemViewHolder, position: Int) {
        val postItem = getItem(position)

        holder.itemView.setOnClickListener {
            onItemClicked?.invoke(postItem)
        }

        holder.bind(postItem)
    }

    inner class MessageItemViewHolder(
        private val binding: MessageCardBinding,
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(messageItem: Message) {
            binding.message = messageItem
            binding.executePendingBindings()
        }
    }
}

private class MessageItemDiffCallback : DiffUtil.ItemCallback<Message>() {
    override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean {
        return oldItem.id == newItem.id
    }

}