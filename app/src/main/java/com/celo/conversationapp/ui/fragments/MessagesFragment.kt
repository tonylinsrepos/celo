package com.celo.conversationapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.adapters.TextViewBindingAdapter.setText
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.celo.conversationapp.R
import com.celo.conversationapp.databinding.FragmentMessagesBinding
import com.celo.conversationapp.models.Message
import com.celo.conversationapp.ui.MessageListAdapter
import com.celo.conversationapp.ui.activities.MainActivity
import com.celo.conversationapp.viewModels.ExtraConversationViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MessagesFragment : Fragment() {

    private val viewModel: ExtraConversationViewModel by navGraphViewModels(R.id.main_nav_graph_xml) {
        defaultViewModelProviderFactory
    }

    private lateinit var binding: FragmentMessagesBinding
    private val messagesFragmentArgs: MessagesFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        this.binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_messages,
            container, false
        )

        this.binding.lifecycleOwner = this.viewLifecycleOwner
        this.binding.viewModel = this.viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //1. load the arguments
        val messageOwnerId = this.messagesFragmentArgs.messageOwnerId
        val ownerType = this.messagesFragmentArgs.messasgeWwnerType

        //2. load and populate the messages
        this.viewModel.getMessages(ownerId = messageOwnerId, ownerType = ownerType).observe(this.viewLifecycleOwner) { messages ->
            this.setupListView(messages)
        }

        //3. setup toolbar
        (activity as? MainActivity)?.apply {
            this.supportActionBar?.title = "$messageOwnerId"

            this.getToolbar().setNavigationOnClickListener {
                findNavController().popBackStack()
                viewModel.cleanMessageOwnerId()
            }
        }

        //4. setup send button
        this.binding.sendButton.setOnClickListener {
            viewModel.sendNewMessage()
            binding.msgEditText.apply {
                setText("")
            }

        }
    }

    private fun setupListView(messages: List<Message>) {
        MessageListAdapter().also { adapter ->
            this.binding.messageList.adapter = adapter
            adapter.submitList(messages)
        }
    }
}