package com.celo.conversationapp.ui.activities

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.celo.conversationapp.R
import com.celo.conversationapp.databinding.ActivityInitBinding
import com.celo.conversationapp.viewModels.BasicConversationViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class InitActivity : AppCompatActivity() {

    private val viewModelBasic: BasicConversationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityInitBinding = DataBindingUtil.setContentView<ActivityInitBinding?>(this, R.layout.activity_init)
        binding.progressBar.isVisible = true
        binding.initMockDataButton.isVisible = false

        //populate the mock data for initialization or navigate to main activity if they have been populated already.
        this.viewModelBasic.isMockDataPopulated.observe(this) { isMockDataPopulated ->

            if (!isMockDataPopulated) {
                binding.progressBar.isVisible = false
                binding.initMockDataButton.apply {
                    this.setOnClickListener {
                        binding.progressBar.isVisible = true
                        binding.initMockDataButton.isVisible = false
                        lifecycleScope.launch {
                            viewModelBasic.populateMockData()
                        }
                    }

                    this.isVisible = true
                }
            } else {
                //store the user name of the current app user
                val name = when (binding.nameEditText.text.toString().trim().isBlank()) {
                    true -> binding.nameEditText.hint
                    false -> binding.nameEditText.text.trim()
                }
                viewModelBasic.setUserName(name)
                MainActivity.start(this)
            }
        }

    }
}