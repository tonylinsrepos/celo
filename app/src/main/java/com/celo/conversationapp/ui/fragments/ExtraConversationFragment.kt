package com.celo.conversationapp.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.celo.conversationapp.R
import com.celo.conversationapp.databinding.FragmentExtraConversationsBinding
import com.celo.conversationapp.models.Conversation
import com.celo.conversationapp.models.ConversationWithLatestMessage
import com.celo.conversationapp.ui.ConversationListAdapter
import com.celo.conversationapp.viewModels.ExtraConversationViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ExtraConversationFragment : Fragment() {

    private val viewModel: ExtraConversationViewModel by navGraphViewModels(R.id.main_nav_graph_xml) {
        defaultViewModelProviderFactory
    }

    private lateinit var binding: FragmentExtraConversationsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        this.binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_extra_conversations,
            container, false
        )

        this.binding.lifecycleOwner = this.viewLifecycleOwner
        this.binding.viewModel = this.viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //populate all or the filtered conversations
        this.viewModel.conversationsAndCasesToDisplay.observe(viewLifecycleOwner) { conversations ->
            setupListView(conversations)
        }

        this.binding.newConversationButton.setOnClickListener {
            //prompt the dialog for the conversation name
            val dialog = NewConversationDialog { newConversationName ->
                viewModel.createConversation(newConversationName)
            }

            dialog.show(childFragmentManager, "")
        }
    }

    private fun setupListView(conversationWithLatestMessages: List<ConversationWithLatestMessage>) {
        ConversationListAdapter().also { adapter ->
            adapter.onItemClicked = this::onConversationClicked
            this.binding.fragmentConversations.conversationList.adapter = adapter
            adapter.submitList(conversationWithLatestMessages)
        }
    }

    private fun onConversationClicked(conversationWithLatestMessages: ConversationWithLatestMessage) {
        Log.d("onConversationClicked", "${conversationWithLatestMessages.latestMessage?.content}")
        val action = ExtraConversationFragmentDirections.actionNavigationExtraToMessagesFragment(
            messageOwnerId = conversationWithLatestMessages.conversation.id,
            messasgeWwnerType = when (conversationWithLatestMessages.conversation) {
                is Conversation -> 0
                else -> 1
            }
        )
        findNavController().navigate(action)
    }
}