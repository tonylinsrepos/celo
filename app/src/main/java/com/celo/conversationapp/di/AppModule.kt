package com.celo.conversationapp.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.celo.conversationapp.BuildConfig
import com.celo.conversationapp.db.*
import com.celo.conversationapp.repositories.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideApplicationContext(@ApplicationContext context: Context): Context {
        return context
    }

    @Provides
    @Singleton
    fun provideCaseRepository(caseDao: CaseDao, messageDao: MessageDao): ICaseRepository {
        return CaseRepository(
            caseDao = caseDao,
            messageDao = messageDao
        )
    }

    @Provides
    @Singleton
    fun provideMessageRepository(messageDao: MessageDao): IMessageRepository {
        return MessageRepository(
            messageDao = messageDao
        )
    }

    @Provides
    @Singleton
    fun provideConversationRepository(conversationDao: ConversationDao, messageDao: MessageDao): IConversationRepository {
        return ConversationRepository(
            conversationDao = conversationDao,
            messageDao = messageDao
        )
    }

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext applicationContext: Context): AppDatabase {
        val builder = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "celo.db"
        )
            .addMigrations(*Migrations.ALL_MIGRATIONS.toTypedArray())

        // Disable Write Ahead Logging for debug so we can easily copy the DB file from the device
        if (BuildConfig.DEBUG) {
            builder.setJournalMode(RoomDatabase.JournalMode.TRUNCATE)
        }

        return builder.build()
    }

    @Provides
    fun provideConversationDao(database: AppDatabase): ConversationDao {
        return database.conversationDao()
    }

    @Provides
    fun provideCaseDao(database: AppDatabase): CaseDao {
        return database.caseDao()
    }

    @Provides
    fun provideMessageDao(database: AppDatabase): MessageDao {
        return database.messageDao()
    }
}