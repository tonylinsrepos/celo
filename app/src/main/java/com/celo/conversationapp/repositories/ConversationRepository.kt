package com.celo.conversationapp.repositories

import com.celo.conversationapp.MOCK_USER_NAME
import com.celo.conversationapp.MESSAGE_OWNER_TYPE_CONVERSATION
import com.celo.conversationapp.db.ConversationDao
import com.celo.conversationapp.db.MessageDao
import com.celo.conversationapp.models.Conversation
import com.celo.conversationapp.models.ConversationWithLatestMessage
import com.celo.conversationapp.models.Message
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ConversationRepository @Inject constructor(
    private val conversationDao: ConversationDao,
    private val messageDao: MessageDao
) : IConversationRepository {

    override suspend fun initConversationsWithFirstMessageContent(conversationsWithMsg: List<Pair<Conversation, String>>) {
        conversationsWithMsg.forEach {
            val conversation = it.first
            val msgString = it.second

            val conversationId = conversationDao.insertConversation(conversation = conversation)

            val msg = Message(
                ownerId = conversationId,
                content = msgString,
                timeStamp = System.currentTimeMillis(),
                senderName = MOCK_USER_NAME,
                ownerType = MESSAGE_OWNER_TYPE_CONVERSATION
            )

            messageDao.insertMessage(msg)
        }
    }

    override fun getAlLConversationsWithLatestMessage(): Flow<List<ConversationWithLatestMessage>> {
        return conversationDao.getConversations().map { conversations ->
            conversations.map { conversation ->
                val message: Message? = messageDao.getLatestMessagesWithOwnerId(ownerId = conversation.id, ownerType = 0).firstOrNull()
                ConversationWithLatestMessage(conversation = conversation, latestMessage = message)
            }
        }.flowOn(Dispatchers.IO)

    }

    override fun getAlLConversationsWithLatestMessageByName(name: String): Flow<List<ConversationWithLatestMessage>> {
        return conversationDao.getConversationsByName(name = "%$name%").map { conversations ->
            conversations.map { conversation ->
                val message: Message? = messageDao.getLatestMessagesWithOwnerId(ownerId = conversation.id, ownerType = 0).firstOrNull()
                ConversationWithLatestMessage(conversation = conversation, latestMessage = message)
            }
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun addConversation(conversation: Conversation) {
        this.conversationDao.insertConversation(conversation)
    }

    override suspend fun deleteAllConversations() {
        conversationDao.deleteAll()
    }
}