package com.celo.conversationapp.repositories

import com.celo.conversationapp.models.Case
import com.celo.conversationapp.models.ConversationWithLatestMessage
import kotlinx.coroutines.flow.Flow

interface ICaseRepository {
    suspend fun initCasesWithFirstMessageContent(casesWithMsg: List<Pair<Case, String>>)
    fun getAllCasesWithLatestMessage(): Flow<List<ConversationWithLatestMessage>>
    fun getAlLCasesWithLatestMessageByName(name: String): Flow<List<ConversationWithLatestMessage>>
    suspend fun deleteAllCases()
}