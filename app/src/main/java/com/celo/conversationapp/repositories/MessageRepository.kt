package com.celo.conversationapp.repositories

import com.celo.conversationapp.db.MessageDao
import com.celo.conversationapp.models.Message
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MessageRepository @Inject constructor(
    private val messageDao: MessageDao
) : IMessageRepository {

    override fun getMessageCount(): Flow<Long> {
        return this.messageDao.getMessageCount()
    }

    override fun getMessagesWithOwnerIdAndOwnerType(ownerId: Long, ownerType: Int): Flow<List<Message>> {
        return this.messageDao.getMessagesWithOwnerId(ownerId, ownerType)
    }

    override suspend fun addMessage(message: Message) {
        this.messageDao.insertMessage(message)
    }

    override suspend fun deleteAllMessages() {
        messageDao.deleteAll()
    }

}