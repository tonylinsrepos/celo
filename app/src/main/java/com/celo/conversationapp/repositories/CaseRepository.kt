package com.celo.conversationapp.repositories

import com.celo.conversationapp.MOCK_USER_NAME
import com.celo.conversationapp.MESSAGE_OWNER_TYPE_CASE
import com.celo.conversationapp.db.CaseDao
import com.celo.conversationapp.db.MessageDao
import com.celo.conversationapp.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class CaseRepository @Inject constructor(
    private val caseDao: CaseDao,
    private val messageDao: MessageDao
) : ICaseRepository {

    override suspend fun initCasesWithFirstMessageContent(casesWithMsg: List<Pair<Case, String>>) {
        casesWithMsg.forEach {
            val case = it.first
            val msgString = it.second

            val caseId = caseDao.insertCase(case = case)

            val msg = Message(
                ownerId = caseId,
                content = msgString,
                timeStamp = System.currentTimeMillis(),
                senderName = MOCK_USER_NAME,
                ownerType = MESSAGE_OWNER_TYPE_CASE
            )

            messageDao.insertMessage(msg)
        }
    }

    override fun getAllCasesWithLatestMessage(): Flow<List<ConversationWithLatestMessage>> {
        return caseDao.getCases().map { cases ->
            cases.map { case ->
                val message: Message? = messageDao.getLatestMessagesWithOwnerId(ownerId = case.id, ownerType = 1).firstOrNull()
                ConversationWithLatestMessage(conversation = case, latestMessage = message)
            }
        }.flowOn(Dispatchers.Default)
    }

    override fun getAlLCasesWithLatestMessageByName(name: String): Flow<List<ConversationWithLatestMessage>> {
        return caseDao.getCasesByName(name = "%$name%").map { cases ->
            cases.map { case ->
                val message: Message? = messageDao.getLatestMessagesWithOwnerId(ownerId = case.id, ownerType = 1).firstOrNull()
                ConversationWithLatestMessage(conversation = case, latestMessage = message)
            }
        }.flowOn(Dispatchers.Default)
    }

    override suspend fun deleteAllCases() {
        caseDao.deleteAll()
    }
}