package com.celo.conversationapp.repositories

import com.celo.conversationapp.models.Conversation
import com.celo.conversationapp.models.ConversationWithLatestMessage
import kotlinx.coroutines.flow.Flow

interface IConversationRepository {
    suspend fun initConversationsWithFirstMessageContent(conversationsWithMsg: List<Pair<Conversation, String>>)
    fun getAlLConversationsWithLatestMessage(): Flow<List<ConversationWithLatestMessage>>
    fun getAlLConversationsWithLatestMessageByName(name: String): Flow<List<ConversationWithLatestMessage>>
    suspend fun addConversation(conversation: Conversation)
    suspend fun deleteAllConversations()

}