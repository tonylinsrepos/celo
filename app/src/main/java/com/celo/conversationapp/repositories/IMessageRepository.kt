package com.celo.conversationapp.repositories

import com.celo.conversationapp.models.Message
import kotlinx.coroutines.flow.Flow

interface IMessageRepository {
    fun getMessageCount(): Flow<Long>
    fun getMessagesWithOwnerIdAndOwnerType(ownerId: Long, ownerType: Int): Flow<List<Message>>
    suspend fun addMessage(message: Message)
    suspend fun deleteAllMessages()
}