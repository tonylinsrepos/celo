package com.celo.conversationapp.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.celo.conversationapp.models.Case
import com.celo.conversationapp.models.Conversation
import com.celo.conversationapp.models.Message
import kotlinx.coroutines.flow.Flow

@Dao
interface ConversationDao {
    @Insert
    suspend fun insertConversation(conversation: Conversation): Long

    @Query("SELECT * FROM conversations")
    fun getConversations(): Flow<List<Conversation>>

    @Query("SELECT * FROM conversations WHERE name LIKE :name ORDER by id")
    fun getConversationsByName(name: String): Flow<List<Conversation>>

    @Query("DELETE FROM conversations")
    suspend fun deleteAll()
}

@Dao
interface CaseDao {
    @Insert
    suspend fun insertCase(case: Case): Long

    @Query("SELECT * FROM cases ORDER by id")
    fun getCases(): Flow<List<Case>>

    @Query("SELECT * FROM cases WHERE name LIKE :name ORDER by id")
    fun getCasesByName(name: String): Flow<List<Case>>

    @Query("DELETE FROM cases")
    suspend fun deleteAll()
}

@Dao
interface MessageDao {
    @Insert
    suspend fun insertMessage(message: Message)

    @Query("SELECT * FROM messages WHERE ownerId = :ownerId AND ownerType = :ownerType ORDER by timeStamp DESC")
    fun getMessagesWithOwnerId(ownerId: Long, ownerType: Int): Flow<List<Message>>

    @Query("SELECT * FROM messages WHERE ownerId = :ownerId AND ownerType = :ownerType ORDER by timeStamp DESC LIMIT 1")
    fun getLatestMessagesWithOwnerId(ownerId: Long, ownerType: Int): List<Message>

    @Query("DELETE FROM messages")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM messages")
    fun getMessageCount(): Flow<Long>
}
