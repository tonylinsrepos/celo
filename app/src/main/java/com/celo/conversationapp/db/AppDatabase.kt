package com.celo.conversationapp.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import com.celo.conversationapp.models.Case
import com.celo.conversationapp.models.Conversation
import com.celo.conversationapp.models.Message


@Database(
    entities = [
        Conversation::class,
        Case::class,
        Message::class
    ],
    version = 2, exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun conversationDao(): ConversationDao
    abstract fun caseDao(): CaseDao
    abstract fun messageDao(): MessageDao
}

object Migrations {
    val ALL_MIGRATIONS = listOf<Migration>(
    )
}