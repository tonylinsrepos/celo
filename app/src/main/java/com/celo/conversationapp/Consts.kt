package com.celo.conversationapp

const val MOCK_USER_NAME = "Tony Lin"

const val KEY_IS_MOCK_DATA_POPULATED = "KEY_IS_MOCK_DATA_POPULATED"
const val KEY_USER_NAME = "KEY_USER_NAME"

const val MESSAGE_OWNER_TYPE_CONVERSATION = 0
const val MESSAGE_OWNER_TYPE_CASE = 1