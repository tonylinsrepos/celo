package com.celo.conversationapp.viewModels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.celo.conversationapp.KEY_USER_NAME
import com.celo.conversationapp.R
import com.celo.conversationapp.models.Conversation
import com.celo.conversationapp.models.Message
import com.celo.conversationapp.repositories.ICaseRepository
import com.celo.conversationapp.repositories.IConversationRepository
import com.celo.conversationapp.repositories.IMessageRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ExtraConversationViewModel @Inject constructor(
    conversationRepository: IConversationRepository,
    caseRepository: ICaseRepository,
    messageRepository: IMessageRepository,
    context: Context
) : BasicConversationViewModel(
    conversationRepository,
    caseRepository,
    messageRepository,
    context
) {

    val newMessageText = MutableLiveData<String>("")
    val isMessageReady = newMessageText.switchMap { newMessage ->
        MutableLiveData<Boolean>().apply {
            this.value = newMessage.trim().isNotBlank()
        }
    }
    private val defaultUserName = context.getString(R.string.default_user_name)

    private fun getUserName(): String {
        return this.sharedPreferences.getString(KEY_USER_NAME, defaultUserName) ?: defaultUserName
    }

    fun sendNewMessage() {
        val msg = Message(
            ownerId = this.messageOwnerId.value!!,
            content = this.newMessageText.value!!,
            timeStamp = System.currentTimeMillis(),
            senderName = this.getUserName(),
            ownerType = this.messageOwnerType
        )

        viewModelScope.launch {
            messageRepository.addMessage(msg)
        }
    }

    fun createConversation(newConversationName: String) {
        val conversation = Conversation(
            id = 0,
            name = newConversationName
        )
        viewModelScope.launch {
            conversationRepository.addConversation(conversation)
        }

    }
}