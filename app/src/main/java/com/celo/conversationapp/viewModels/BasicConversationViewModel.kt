package com.celo.conversationapp.viewModels

import android.content.Context
import androidx.lifecycle.*
import com.celo.conversationapp.KEY_IS_MOCK_DATA_POPULATED
import com.celo.conversationapp.KEY_USER_NAME
import com.celo.conversationapp.R
import com.celo.conversationapp.buildMockCases
import com.celo.conversationapp.buildMockConversations
import com.celo.conversationapp.models.ConversationWithLatestMessage
import com.celo.conversationapp.models.Message
import com.celo.conversationapp.repositories.ICaseRepository
import com.celo.conversationapp.repositories.IConversationRepository
import com.celo.conversationapp.repositories.IMessageRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

@HiltViewModel
open class BasicConversationViewModel @Inject constructor(
    protected val conversationRepository: IConversationRepository,
    protected val caseRepository: ICaseRepository,
    protected val messageRepository: IMessageRepository,
    context: Context
) : ViewModel() {

    protected val sharedPreferences = context.getSharedPreferences("ConversationApp", Context.MODE_PRIVATE)

    private val searchText = context.resources.getText(R.string.search)
    private val resultFoundText = context.getText(R.string.result_found)

    private val _isDataMockPopulated = MutableLiveData<Boolean>(false).apply {
        this.value = this@BasicConversationViewModel.sharedPreferences.getBoolean(KEY_IS_MOCK_DATA_POPULATED, false)
    }
    val isMockDataPopulated: LiveData<Boolean> = _isDataMockPopulated

    val isConversationsChecked = MutableLiveData<Boolean>(true)
    val isCasesChecked = MutableLiveData<Boolean>(true)

    val nameSearchKey = MutableLiveData<String>("")
    val conversationsAndCasesToDisplay: LiveData<List<ConversationWithLatestMessage>> = MediatorLiveData<Triple<String?, Boolean?, Boolean?>>().apply {
        addSource(nameSearchKey) { this.postValue(Triple(it, isConversationsChecked.value, isCasesChecked.value)) }
        addSource(isConversationsChecked) { this.postValue(Triple(nameSearchKey.value, it, isCasesChecked.value)) }
        addSource(isCasesChecked) { this.postValue(Triple(nameSearchKey.value, isConversationsChecked.value, it)) }
        addSource(messageRepository.getMessageCount().asLiveData()) {
            this.postValue(
                Triple(
                    nameSearchKey.value,
                    isConversationsChecked.value,
                    isCasesChecked.value
                )
            )
        }
    }.switchMap {
        val searchKey = it.first ?: ""
        val isConversationsChecked = it.second ?: true
        val isCasesChecked = it.third ?: true

        val flowPair = when (searchKey.isNotBlank()) {
            true -> {
                val allConversationsFlow = conversationRepository.getAlLConversationsWithLatestMessageByName(name = searchKey)
                val allCasesFlow = caseRepository.getAlLCasesWithLatestMessageByName(name = searchKey)
                Pair(allConversationsFlow, allCasesFlow)
            }
            false -> {
                val allConversationsFlow = conversationRepository.getAlLConversationsWithLatestMessage()
                val allCasesFlow = caseRepository.getAllCasesWithLatestMessage()
                Pair(allConversationsFlow, allCasesFlow)
            }
        }

        val flows = mutableListOf<Flow<List<ConversationWithLatestMessage>>>()
        if (isConversationsChecked) {
            flows.add(flowPair.first)
        }

        if (isCasesChecked) {
            flows.add(flowPair.second)
        }

        when (flows.isNotEmpty()) {
            true -> {
                flows.reduce { acc, flow ->
                    acc.combine(flow) { conversations, cases ->
                        (conversations + cases).sortedByDescending {
                            it.latestMessage?.timeStamp
                        }
                    }
                }.asLiveData()
            }

            false -> {
                MutableLiveData<List<ConversationWithLatestMessage>>().apply {
                    this.value = emptyList()
                }
            }
        }

    }

    val conversationSearchResultCount: LiveData<String> = conversationsAndCasesToDisplay.switchMap {
        MutableLiveData<String>().apply {
            when (it.isNotEmpty()) {
                true -> "${it.size} $resultFoundText"
                false -> searchText.toString()
            }.let {
                postValue(it)
            }
        }
    }

    protected val messageOwnerId = MutableLiveData<Long?>()
    protected var messageOwnerType: Int = 0
    private val messages: LiveData<List<Message>> = messageOwnerId.switchMap { messageOwnerId ->
        messageOwnerId?.let {
            messageRepository.getMessagesWithOwnerIdAndOwnerType(ownerId = it, ownerType = this.messageOwnerType).asLiveData()
        } ?: MutableLiveData(emptyList())
    }

    suspend fun populateMockData() {
        this.messageRepository.deleteAllMessages()

        this.conversationRepository.let {
            it.deleteAllConversations()
            it.initConversationsWithFirstMessageContent(buildMockConversations())
        }

        this.caseRepository.let {
            it.deleteAllCases()
            it.initCasesWithFirstMessageContent(buildMockCases())
        }

        this.sharedPreferences.edit().also {
            it.putBoolean(KEY_IS_MOCK_DATA_POPULATED, true).apply()
            this@BasicConversationViewModel._isDataMockPopulated.value = true
        }

    }

    fun getMessages(ownerId: Long, ownerType: Int): LiveData<List<Message>> {
        this.messageOwnerType = ownerType
        this.messageOwnerId.postValue(ownerId)

        return this.messages
    }

    fun cleanMessageOwnerId() {
        this.messageOwnerId.value = null
    }

    fun setUserName(name: CharSequence) {
        this.sharedPreferences.edit().putString(KEY_USER_NAME, name.toString()).apply()
    }
}