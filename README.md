# Hi!

Welcome to the Celo technical exercise. The idea is for us to get an understanding of your coding style, program design and architectural decisions, as well 
as for you to have some exposure to the kinds of problems we solve for our customers.

Once you have completed the exercise, we will walk through your solution together and explore your high and low level decisions/implementations.


# The exercise

Contained in this project is a basic app that displays a list of conversations to the user (think Whatsapp or Slack). There is also button that allows 
switching between conversation 'types' (more on this below).

The application is quite rudimentary, so we'd like to give our user some more tools! 

We would like you to add 2 pieces of functionality (if you get stuck or run out of time, no problem, we can talk through it during the interview):

### 1:
Implement simple filtering, where the user can type into a search box and see results filtered by the name of a conversation.

### 2:
We would like to add a second type of conversation to the app, allowing the user to switch between the two. This type will be a 'Case', with the only difference
being that there is a patient ID attached. When the toggle button is pushed, the data sets are switched, respecting any filter term currently in use. It might 
be easier for the user to differentiate between conversations and cases if they are displayed with different colors.

# Extras

Feel free to change, delete or add classes, interfaces, layouts, tests and dependencies as you see fit. 

If you have any questions or suggestions, please bring them up in our follow up interview.